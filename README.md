# README #

This is an example Go (golang) project to handling exchange-rates

### How do I build it? ###

* Download Golang
* (Optional) Download LiteIDE (Go IDE)
* set GOROOT (the go sdk/binary path)
* set GOPATH (for example /user/go-local)
* go into $GOPATH
* git checkout into $GOPATH/src
* cd into the app directory (goex)
* 1) go.exe get -v . $GOPATH < the goopath value has to be here
* 2) go build -i the-source-dir-path

### How do I run it? ###

simply run the generated file :)

it supports optional commandline arguments (call executable with -h to get the default values):

* port
* loglevel

### How do I test it? ###

* with your browser, set the url to http://localhost:8080/rs/private/currencies
* with curl or similar tools

### I dont want to build it, where is the binary? ###

Go to the "Downloads" section, and get the x86-64 file, in it, the Linux and Windows binaries can be found :)
