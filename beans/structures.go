// BeanStructures
package beans

type CurrencyPairRB struct {
	From string `json:"fromCurrency"`
	To   string `json:"toCurrency"`
}

type ExchangeRateRB struct {
	CurrencyPair       CurrencyPairRB `json:"currencyPair"`
	ExchangeRate       float64        `json:"exchangeRate"`
	ExchangeRateStatus string         `json:"exchangeRateStatus"`
	CreatedAt          string         `json:"createdAt"`
}

type ExchangeRates struct {
	Id        int     `json:"id"`
	HufToEur  float64 `json:"hufToEur"`
	HufToUsd  float64 `json:"hufToUsd"`
	UsdToEur  float64 `json:"usdToEur"`
	EurToHuf  float64 `json:"eurToHuf"`
	UsdToHuf  float64 `json:"usdToHuf"`
	EurToUsd  float64 `json:"eurToUsd"`
	Date      string  `json:"date"`
	CreatedAt string  `json:"createdAt"`
}

type GetRateRB struct {
	Date         string         `json:"date"`
	CurrencyPair CurrencyPairRB `json:"currencyPair"`
}

type Status int

const (
	OK Status = 1 + iota
	UNSUPPORTED_CURRENCY_CODE
)

var statuses = [...]string{
	"OK",
	"UNSUPPORTED_CURRENCY_CODE",
}

func (m Status) String() string { return statuses[m-1] }

type StringListWrapper struct {
	Entities []string `json:"entities"`
}

type ExchangeRateRBListWrapper struct {
	Entities []ExchangeRateRB `json:"entities"`
}

type CurrencyPairRBListWrapper struct {
	Entities []CurrencyPairRB `json:"entities"`
}

type RatesListEntity struct {
	id    string
	Date  string
	Rates []RatesDataEntity
}
type RatesDataEntity struct {
	Currency string
	Rate     float64
	Unit     int
}
