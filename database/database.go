package database

import (
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gopkg.in/mgo.v2"
)

type dbConnection struct {
	DBSession mgo.Session
	Alive     bool
	Error     error
}

var myDBConnection *dbConnection
var once sync.Once

func Init(mongoHost string) {
	once.Do(func() {
		myDBConnection = &dbConnection{Alive: false, Error: nil}
		go cDB(mongoHost)
	})
}

func GetInstance() *dbConnection {
	return myDBConnection
}

func cDB(mongoHost string) {

	count := 0
	for {
		myDBConnection = connectToDatabase(mongoHost)
		if myDBConnection.Alive {
			log.Infof("connect to MongoDB %s successfull, after %d tries", mongoHost, count)
			break
		} else {
			count++
			log.Errorf("connect to MongoDB %s failed, tried %d times", mongoHost, count)
			time.Sleep(time.Second * 5)
		}
	}
}

func connectToDatabase(mongoHost string) *dbConnection {
	session, err := mgo.Dial(mongoHost)

	if err != nil {
		log.Debugf("ConnectDatabase failed with: %s", err.Error())
		return &dbConnection{Alive: false, Error: err}
	}
	session.SetMode(mgo.Monotonic, true)
	return &dbConnection{Alive: true, Error: nil, DBSession: *session}
}
