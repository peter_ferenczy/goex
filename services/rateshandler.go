package services

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	log "github.com/sirupsen/logrus"

	"goex/beans"
	"goex/database"
	"goex/mnbapi"
	"goex/validator"

	"github.com/emicklei/go-restful"
	"github.com/shopspring/decimal"
)

const oldestSupportedDate = "2006-01-01" // it is used for the populate call
const maxFallbackDays = 5                // the max days go back in the past, to find a rate

// api/currencies
func ListCurrenciesForWS(request *restful.Request, response *restful.Response) {
	currencies, err := getCurrencyListing()
	if err != nil {
		response.AddHeader("Content-Type", "text/plain")
		response.WriteErrorString(http.StatusInternalServerError, err.Error())
	} else {
		response.WriteEntity(beans.StringListWrapper{Entities: currencies})
	}
}

// currencies
func ListCurrencies(request *restful.Request, response *restful.Response) {
	currencies, err := getCurrencyListing()
	if err != nil {
		response.AddHeader("Content-Type", "text/plain")
		response.WriteErrorString(http.StatusInternalServerError, err.Error())
	} else {
		response.WriteEntity(currencies)
	}

}

// populate
func PopulateHistoricExchageRates(request *restful.Request, response *restful.Response) {
	// we should somehow determine/block call to this service while it is running
	// mutex?
	ch := make(chan rateElement, 50)
	go getHistoricalCurrencyHashMapUntilToday(oldestSupportedDate, ch)

	go saveHistoricalRates(ch)
	response.WriteHeader(http.StatusAccepted)

}

//	rates/info
func GetLatestRateInfo(request *restful.Request, response *restful.Response) {

	rb := new(beans.CurrencyPairRBListWrapper)

	err := request.ReadEntity(&rb)
	if err == nil {
		log.Debugf("entiries input count: %d", len(rb.Entities))

		lw := beans.ExchangeRateRBListWrapper{Entities: getV2ExchangeRates(rb.Entities)}
		response.WriteEntity(lw)
	} else {
		response.AddHeader("Content-Type", "text/plain")
		response.WriteErrorString(http.StatusBadRequest, err.Error())
	}
}

// rates/latest/{fromCurrency}/{toCurrency}
func GetLatestRate(request *restful.Request, response *restful.Response) {

	fromCurrency := request.PathParameter("fromCurrency")
	toCurrency := request.PathParameter("toCurrency")

	exRateDecimal := decimalOne()
	currencyMap, err := getCurrencyHashMap()

	if err == nil {

		exRateDecimal = getRateFromMap(fromCurrency, toCurrency, currencyMap)

		response.AddHeader("Content-Type", "text/plain")
		exRate, _ := exRateDecimal.Float64()
		response.WriteEntity(exRate)
	} else {
		response.AddHeader("Content-Type", "text/plain")
		response.WriteErrorString(http.StatusInternalServerError, err.Error())
	}

}

// rates/oldest
func GetOldestExchangeRates(request *restful.Request, response *restful.Response) {
	oldest, _ := time.Parse("2006-01-01", oldestSupportedDate)
	result, err := getV1ExchangeRates(oldest)

	if err != nil {
		response.AddHeader("Content-Type", "text/plain")
		response.WriteErrorString(http.StatusInternalServerError, err.Error())
	} else {
		response.WriteEntity(result)
	}
}

// rates/{dateAsString}
func GetRatesAt(request *restful.Request, response *restful.Response) {
	dateRequest := request.PathParameter("dateAsString")
	log.Infof("GetRatesAt with param %s", dateRequest)

	layout := "2006-01-02"

	parsedDate, err := time.Parse(layout, dateRequest)

	db := database.GetInstance()
	if !db.Alive {
		log.Warn("mng db out of reach, proxy mnb calls")
	}

	if err == nil {
		// get data
		er, dataError := getV1ExchangeRates(parsedDate)
		if dataError == nil {
			response.WriteEntity(er)
		} else {
			response.AddHeader("Content-Type", "text/plain")
			response.WriteErrorString(http.StatusBadRequest, dataError.Error())
		}
	} else {
		response.AddHeader("Content-Type", "text/plain")
		response.WriteErrorString(http.StatusBadRequest, err.Error())
	}

}

// rates
func GetRates(request *restful.Request, response *restful.Response) {
	log.Infof("GetRates")

	rb := new(beans.GetRateRB)

	err := request.ReadEntity(&rb)

	if err == nil {
		log.WithField("GetRateRB", rb).Debug("ReadEntity done successfully")
		if validator.GetRatesInputValid(rb) {
			response.WriteHeaderAndEntity(http.StatusOK, getRatesHandleValidInput(rb))
		} else {
			log.WithField("GetRateRB", rb).Error("Invalid input")
			response.WriteErrorString(http.StatusBadRequest, "Invalid input data")
		}
	} else {
		log.Infof("ReadEntity failed %s", err.Error())
		response.WriteError(http.StatusInternalServerError, err)
	}
}

// this function gets the currency listing either from db or directly from MNB
func getCurrencyListing() ([]string, error) {
	myDB := database.GetInstance()
	if myDB.Alive {
		var currencyList []string
		session := myDB.DBSession.New()
		defer session.Close()

		rates, err := getRatesByDateWithFallback(correctDateForRateQuery(time.Now()), session, 0)
		if err == nil {
			log.Debugf("rates found in db, currency count %d", len(rates.Rates))
			for _, rate := range rates.Rates {
				currencyList = append(currencyList, rate.Currency)
			}
		}
		if len(currencyList) > 1 {
			return currencyList, err
		} else {
			return mnbapi.GetCurrencyListing()
		}

	} else {
		return mnbapi.GetCurrencyListing()
	}

}

// this function extracts the requested exchangeRate from the exchangeRateMap (currenyMap)
func getRateFromMap(fromCurrency string, toCurrency string, currencyMap map[string]string) decimal.Decimal {
	frm, fok := currencyMap[fromCurrency]
	to, tok := currencyMap[toCurrency]

	log.Debugf("rates map has %d items", len(currencyMap))
	exRateDecimal := decimalOne()
	HUFfromRate := decimalOne()
	HUFtoRate := decimalOne()

	var err error
	if fok {
		err = nil
		HUFfromRate, err = decimal.NewFromString(frm)
		log.Debugf("found HUFFrom for %s, error: %s", frm, err)
	}
	if tok {
		err = nil
		HUFtoRate, err = decimal.NewFromString(to)
		log.Debugf("found HUFTo for %s, error: %s", to, err)
	}

	log.Debugf("found rates: HUF to %s: %s, HUF to %s: %s ", fromCurrency, HUFfromRate.String(), toCurrency, HUFtoRate.String())
	currencyFromInHUF := decimalOne().DivRound(HUFfromRate, 20)
	currencyToInHUF := decimalOne().DivRound(HUFtoRate, 20)
	exRateDecimal = currencyToInHUF.DivRound(currencyFromInHUF, 20)

	return exRateDecimal
}

// this function prepares the exchange rates map (currencyMap), directly from MNB
// it can be easily rewritten to DB access later on
func getCurrencyHashMap() (map[string]string, error) {
	myMap := map[string]string{}

	myDB := database.GetInstance()
	if myDB.Alive {
		session := myDB.DBSession.New()
		defer session.Close()

		rates, err := getRatesByDateWithFallback(correctDateForRateQuery(time.Now()), session, 0)
		if err == nil {
			for _, rate := range rates.Rates {
				myMap[rate.Currency] = fmt.Sprintf("%.6f", rate.Rate)
			}
		}

		// if not, try to get the data from mnb.. eventually this could be a config setting
	} else {
		sc := mnbapi.NewMNBArfolyamServiceSoap("http://www.mnb.hu/arfolyamok.asmx", false, nil)

		cresponse, err := sc.GetCurrentExchangeRates(nil)

		if err != nil {
			return nil, err
		} else {

			// len(cresponse.Days) must be exact 1 if not, fail!
			for _, rates := range cresponse.Days[0].Rates {
				myMap[rates.Curr] = strings.Replace(rates.Value, ",", ".", -1)
			}

		}
	}
	return myMap, nil

}

// this function creates a decimal.Decimal with value 1
func decimalOne() decimal.Decimal {
	one, _ := decimal.NewFromString("1")
	return one
}

func getRatesByDateWithFallback(requestDate time.Time, session *mgo.Session, iterationCounter int) (beans.RatesListEntity, error) {
	c := session.DB("exchange-rates").C("rates_for_v1_functions")
	result := beans.RatesListEntity{}
	err := c.Find(bson.M{"date": requestDate.Format("2006-01-02")}).One(&result)
	log.Infof("getRatesByDateWithFallback called times: %d, with date this time: %s", iterationCounter, requestDate.Format("2006-01-02"))
	if err != nil {
		requestDate = requestDate.AddDate(0, 0, -1)
		iterationCounter++
		if iterationCounter > maxFallbackDays {
			return beans.RatesListEntity{}, errors.New("No data found in the DB, even with fallback. Giving up..")
		}
		return getRatesByDateWithFallback(correctDateForRateQuery(requestDate), session, iterationCounter)
	}
	return result, nil
}

// this function generates the V1 style exchangeRates for the specified date
func getV1ExchangeRates(requestDate time.Time) (beans.ExchangeRates, error) {

	myMap := map[string]string{}

	correctedRequestDate := correctDateForRateQuery(requestDate)

	db := database.GetInstance()
	if db.Alive {
		session := db.DBSession.New()
		defer session.Close()

		result, err := getRatesByDateWithFallback(correctedRequestDate, session, 0)
		if err != nil {
			log.Warnf("no data can be retreived from db: %s with param: %s, fallback to MNB", err.Error(), correctedRequestDate.Format("2006-01-02"))
			// we have to try to get the nearest date from the db

			currencyMap, err := getHistoricalCurrencyHashMap(correctedRequestDate.Format("2006-01-02"))
			if err != nil {
				log.Warnf("no data can be retreived from MNB neither: %s with param: %s", err.Error(), correctedRequestDate.Format("2006-01-02"))
				return beans.ExchangeRates{}, err
			}
			myMap = currencyMap
		} else {
			correctedRequestDate, _ = time.Parse("2006-01-02", result.Date)
			for _, rateData := range result.Rates {
				myMap[rateData.Currency] = fmt.Sprintf("%.20f", rateData.Rate)
			}
		}

	} else {
		currencyMap, err := getHistoricalCurrencyHashMap(correctedRequestDate.Format("2006-01-02"))
		if err != nil {
			return beans.ExchangeRates{}, err
		}
		myMap = currencyMap
	}

	return mapToV1Format(myMap, requestDate, correctedRequestDate), nil
}

func mapToV1Format(myMap map[string]string, requestDate, correctedRequestDate time.Time) beans.ExchangeRates {

	hufToEurRate, _ := getRateFromMap("HUF", "EUR", myMap).Float64()
	HufToUsdRate, _ := getRateFromMap("HUF", "USD", myMap).Float64()
	usdToEurRate, _ := getRateFromMap("USD", "EUR", myMap).Float64()
	eurToHufRate, _ := getRateFromMap("EUR", "HUF", myMap).Float64()
	usdToHufRate, _ := getRateFromMap("USD", "HUF", myMap).Float64()
	eurToUsdRate, _ := getRateFromMap("EUR", "USD", myMap).Float64()

	return beans.ExchangeRates{
		Id:        1,
		HufToEur:  hufToEurRate,
		HufToUsd:  HufToUsdRate,
		UsdToEur:  usdToEurRate,
		EurToHuf:  eurToHufRate,
		UsdToHuf:  usdToHufRate,
		EurToUsd:  eurToUsdRate,
		Date:      requestDate.Format("2006-01-02"),
		CreatedAt: correctedRequestDate.Format("2006-01-02"),
	}
}

func correctDateForRateQuery(requestDate time.Time) time.Time {

	correctedRequestDate := requestDate

	if requestDate.After(time.Now()) {
		log.Warnf("cannot determine rates in the future (%s), set the date to now", requestDate.Format("2006-01-02"))
		correctedRequestDate = time.Now()
	}

	if requestDate.Weekday() == time.Sunday {
		//sunday
		correctedRequestDate = requestDate.AddDate(0, 0, -2)
		log.Warnf("no rates at sundays set the date to %s", correctedRequestDate.Format("2006-01-02"))
	}

	if requestDate.Weekday() == time.Saturday {
		//saturday
		correctedRequestDate = requestDate.AddDate(0, 0, -1)
		log.Warnf("no rates at saturdays set the date to %s", correctedRequestDate.Format("2006-01-02"))
	}

	return correctedRequestDate
}

// this function generates an exchangeRate map (with the currencies EUR and USD only)
// for the selected date
func getHistoricalCurrencyHashMap(datum string) (map[string]string, error) {

	myMap := map[string]string{}

	sc := mnbapi.NewMNBArfolyamServiceSoap("http://www.mnb.hu/arfolyamok.asmx", false, nil)

	scb := mnbapi.GetExchangeRatesRequestBody{StartDate: datum, EndDate: datum, CurrencyNames: "EUR,USD"}

	luann, err := sc.GetExchangeRates(&scb)

	if err != nil {
		log.Printf("soap call %s", err.Error())
		return nil, err
	} else {

		// len(luann.Days) must be exact 1 if not, fail!
		if len(luann.Days) == 1 {
			for _, rates := range luann.Days[0].Rates {
				myMap[rates.Curr] = strings.Replace(rates.Value, ",", ".", -1)
			}
		} else {
			return nil, errors.New(fmt.Sprintf("Illegal parameter value, or no data found: %s", datum))

		}
	}
	return myMap, nil
}

// this function generates an exchangeRate map (with the currencies EUR and USD only)
// from the selected date, until today
// this method should be synchronized, must not called paralell..
// if already running, an error should be returned (or panic?)
func getHistoricalCurrencyHashMapUntilToday(datum string, c chan rateElement) {

	sc := mnbapi.NewMNBArfolyamServiceSoap("http://www.mnb.hu/arfolyamok.asmx", false, nil)

	// get the currency listing
	clist, _ := getCurrencyListing()

	currencyFlatList := strings.Join(clist, ",")

	log.Debugf("getHistoricalCurrencyHashMapUntilToday - clist count: %d, the list itself: %s", len(clist), currencyFlatList)
	// third step, GetExchangeRates with all te currencies available

	scb := mnbapi.GetExchangeRatesRequestBody{StartDate: datum, EndDate: time.Now().Format("2006-01-02"), CurrencyNames: currencyFlatList}

	luann, err := sc.GetExchangeRates(&scb)

	if err != nil {
		log.Printf("soap call %s", err.Error())
		// what to do here??
	} else {

		// len(luann.Days) must be exact 1 if not, fail!
		for _, exrday := range luann.Days {
			myMap := map[string]string{}
			for _, rates := range exrday.Rates {
				myMap[rates.Curr] = strings.Replace(rates.Value, ",", ".", -1)
			}
			// we should put this into a chanel
			// and on the caller side, should be saved into the db
			c <- rateElement{Date: exrday.Date, RatesList: myMap}
		}

	}
}

// get rates for the given currencyPair (date will be ignored at now)
func getRatesHandleValidInput(getRateRB *beans.GetRateRB) (result []beans.ExchangeRateRB) {

	currencyMap, err := getCurrencyHashMap()
	if err != nil {
		return []beans.ExchangeRateRB{}
	}
	rs := []beans.ExchangeRateRB{}

	hufToEurRate, _ := getRateFromMap(getRateRB.CurrencyPair.From, getRateRB.CurrencyPair.To, currencyMap).Float64()

	rs = []beans.ExchangeRateRB{
		{
			CurrencyPair:       beans.CurrencyPairRB{From: getRateRB.CurrencyPair.From, To: getRateRB.CurrencyPair.To},
			ExchangeRate:       hufToEurRate,
			ExchangeRateStatus: beans.OK.String(),
			CreatedAt:          time.Now().Format("2006-01-02"),
		},
	}

	return rs
}

// get V2 type exchange rates, for the given currencyPair
func getV2ExchangeRates(cPairs []beans.CurrencyPairRB) (result []beans.ExchangeRateRB) {

	currencyMap, _ := getCurrencyHashMap()

	rs := []beans.ExchangeRateRB{}

	for _, element := range cPairs {
		exRate, _ := getRateFromMap(element.From, element.To, currencyMap).Float64()
		rs = append(rs, beans.ExchangeRateRB{
			CurrencyPair:       beans.CurrencyPairRB{From: element.From, To: element.To},
			CreatedAt:          time.Now().Format("2006-01-02"),
			ExchangeRate:       exRate,
			ExchangeRateStatus: beans.OK.String()})
	}

	return rs
}

type rateElement struct {
	Date      string
	RatesList map[string]string
}

func saveHistoricalRates(ch chan rateElement) {

	session := database.GetInstance().DBSession.New()
	defer session.Close()
	c := session.DB("exchange-rates").C("rates_for_v1_functions")

	for i := range ch {
		saveHistoricalRate(i, c)
	}

}

func saveHistoricalRate(element rateElement, c *mgo.Collection) {

	date := element.Date
	log.Debugf("checking date: %s", date)
	// we should try to get the data from DB first, and append/update values if there is any

	dbResult := beans.RatesListEntity{}
	dbGetErr := c.Find(bson.M{"date": date}).One(&dbResult)

	var ratesList []beans.RatesDataEntity
	result := beans.RatesListEntity{Date: date}
	if dbGetErr == nil {
		// there is already something in the db for this date
		ratesList = dbResult.Rates
		log.Debugf("for date already something in the db: %s, count: %d", date, len(ratesList))
	}
	for rKey, dailyRate := range element.RatesList {
		rateValueDecimal, err := decimal.NewFromString(dailyRate)
		if err != nil {
			log.Warnf("some values are invalid, data will be skipped. c: %s, dr: %s", rKey, dailyRate)
		} else {

			exRate, _ := rateValueDecimal.Float64()
			if !currencyInList(rKey, ratesList) {
				ratesList = append(ratesList, beans.RatesDataEntity{Currency: rKey, Rate: exRate})
			}
		}
	}
	// write data on daily basis
	if len(ratesList) > 0 {
		for _, re := range ratesList {
			result.Rates = append(result.Rates, re)
		}
	}

	var dbErr error
	if dbGetErr != nil {
		dbErr = c.Insert(result)
	} else {
		dbErr = c.Update(bson.M{"date": date}, result)
	}

	iErr := c.EnsureIndex(mgo.Index{Key: []string{"date"}, Background: true})
	if iErr != nil {
		log.Warnf("indexing seems to be failing: %s", iErr)
	}

	if dbErr != nil {
		log.Fatalf("something happened, we were not able to save the data, for date: %s, %s", date, dbErr.Error())
	} else {
		log.Infof("all data save sucessfully for date: %s", date)
	}
}

func currencyInList(currency string, list []beans.RatesDataEntity) bool {
	for _, b := range list {
		if b.Currency == currency {
			return true
		}
	}
	return false
}
