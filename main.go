package main

import (
	"flag"
	"fmt"
	"goex/database"
	"goex/services"
	"net/http"
	"time"

	"github.com/emicklei/go-restful"
	log "github.com/sirupsen/logrus"
)

func main() {

	portPtr := flag.Int("port", 8080, "http port to listen on")
	loglevelPtr := flag.Int("loglevel", 2, "0 Only errors, 1 Info, 2 Debug")
	mongoPtr := flag.String("mongodb", "192.168.99.100:32780", "the mongoDB database host")
	flag.Parse()

	//restful.Filter(globalLogging)

	database.Init(*mongoPtr)

	restful.Add(registerExchangeRateEndpoints())
	restful.Add(registerHealthcheckEndpoints())
	switch *loglevelPtr {
	case 0:
		log.SetLevel(log.ErrorLevel)
	case 1:
		log.SetLevel(log.InfoLevel)
	case 2:
		log.SetLevel(log.DebugLevel)
	default:
		log.SetLevel(log.InfoLevel)
	}

	log.Infof("start listening on localhost:%d", *portPtr)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *portPtr), nil))
}

func registerHealthcheckEndpoints() *restful.WebService {
	ws := new(restful.WebService)
	ws.
		Path("/status").
		Consumes(restful.MIME_XML, restful.MIME_JSON).
		Produces(restful.MIME_JSON, restful.MIME_XML)

	// install a webservice filter (processed before any route)
	ws.Filter(webserviceLogging).Filter(measureTime)

	ws.Route(ws.GET("healthcheck").Filter(routeLogging).Filter(NewCountFilter().routeCounter).To(HealthCheck))
	return ws
}

func HealthCheck(request *restful.Request, response *restful.Response) {

	response.AddHeader("Content-Type", "text/plain")

	response.WriteEntity(fmt.Sprintf("healthy, DB within reach: %t", database.GetInstance().Alive))

}

// prepares the rest endpoints
func registerExchangeRateEndpoints() *restful.WebService {
	ws := new(restful.WebService)
	ws.
		Path("/rs/private/").
		Consumes(restful.MIME_XML, restful.MIME_JSON).
		Produces(restful.MIME_JSON, restful.MIME_XML)

	// install a webservice filter (processed before any route)
	ws.Filter(webserviceLogging).Filter(measureTime)

	ws.Route(ws.POST("rates").Filter(routeLogging).Filter(NewCountFilter().routeCounter).To(services.GetRates))

	ws.Route(ws.GET("rates/{dateAsString}").Filter(routeLogging).Filter(NewCountFilter().routeCounter).To(services.GetRatesAt))

	ws.Route(ws.GET("rates/oldest").Filter(routeLogging).Filter(NewCountFilter().routeCounter).To(services.GetOldestExchangeRates))
	ws.Route(ws.GET("rates/latest/{fromCurrency}/{toCurrency}").Filter(routeLogging).Filter(NewCountFilter().routeCounter).To(services.GetLatestRate))
	ws.Route(ws.POST("rates/info").Filter(routeLogging).Filter(NewCountFilter().routeCounter).To(services.GetLatestRateInfo))

	ws.Route(ws.POST("populate").Filter(routeLogging).Filter(NewCountFilter().routeCounter).To(services.PopulateHistoricExchageRates))
	ws.Route(ws.GET("currencies").Filter(routeLogging).Filter(NewCountFilter().routeCounter).To(services.ListCurrencies))
	ws.Route(ws.GET("api/currencies").Filter(routeLogging).Filter(NewCountFilter().routeCounter).To(services.ListCurrenciesForWS))

	return ws
}

// Global Filter
func globalLogging(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	log.Infof("[global-filter (logger)] %s,%s", req.Request.Method, req.Request.URL)
	chain.ProcessFilter(req, resp)
}

// WebService Filter
func webserviceLogging(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	log.Infof("[webservice-filter (logger)] %s,%s", req.Request.Method, req.Request.URL)
	chain.ProcessFilter(req, resp)
}

// WebService (post-process) Filter (as a struct that defines a FilterFunction)
func measureTime(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	now := time.Now()
	chain.ProcessFilter(req, resp)
	log.Infof("[webservice-filter (timer)] %v", time.Now().Sub(now))
}

// Route Filter (defines FilterFunction)
func routeLogging(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	log.Infof("[route-filter (logger)] %s,%s", req.Request.Method, req.Request.URL)
	chain.ProcessFilter(req, resp)
}

// Route Filter (as a struct that defines a FilterFunction)
// CountFilter implements a FilterFunction for counting requests.
type CountFilter struct {
	count   int
	counter chan int // for go-routine safe count increments
}

// NewCountFilter creates and initializes a new CountFilter.
func NewCountFilter() *CountFilter {
	c := new(CountFilter)
	c.counter = make(chan int)
	go func() {
		for {
			c.count += <-c.counter
		}
	}()
	return c
}

// routeCounter increments the count of the filter (through a channel)
func (c *CountFilter) routeCounter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	c.counter <- 1
	log.Infof("[route-filter (counter)] count:%d", c.count)
	chain.ProcessFilter(req, resp)
}
