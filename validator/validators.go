package validator

import "goex/beans"

// validates some input fields
func GetRatesInputValid(a *beans.GetRateRB) bool {
	if a.Date == "" {
		return false
	}

	if a.CurrencyPair.From == "" || a.CurrencyPair.To == "" {
		return false
	}

	return true
}

func ValidateCurrency(currencies []string, allCurrencies[]string) (map[string]bool, error) {
	result := map[string]bool{}
	for _, currency := range currencies {
		if currencyInSimpleList(currency, allCurrencies) {
			result[currency] = true
		} else {
			result[currency] = false
		}
	}
	return result, nil
}

func currencyInSimpleList(currency string, list []string) bool {
	for _, b := range list {
		if b == currency {
			return true
		}
	}
	return false
}
