package mnbapi

import (
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"errors"
	"io/ioutil"
	"net"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

// against "unused imports"
var _ time.Time
var _ xml.Name

type Char int32

const ()

type Duration *Duration

const ()

type Guid string

const ()

// **** internal structure ****

type MNBCurrencies struct {
	CurrencyList Currencies `xml:"Currencies"`
}

type Currencies struct {
	Currency []string `xml:"Curr"`
}

type MNBCurrentExchangeRates struct {
	Days []EXRDay `xml:"Day"`
}

type MNBExchangeRates struct {
	Days []EXRDay `xml:"Day"`
}

type EXRDay struct {
	Date  string `xml:"date,attr"`
	Rates []Rate `xml:"Rate"`
}

type Rate struct {
	Value string `xml:",chardata"`
	Unit  string `xml:"unit,attr"`
	Curr  string `xml:"curr,attr"`
}

// **** internal structure ****

type GetCurrenciesRequestBody struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetCurrenciesRequest"`
}

type GetCurrenciesResponse struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetCurrenciesResponse"`

	GetCurrenciesResult []byte `xml:"GetCurrenciesResult,omitempty"`
}

type GetCurrencyUnitsRequestBody struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetCurrencyUnitsRequest"`

	CurrencyNames string `xml:"currencyNames,omitempty"`
}

type GetCurrencyUnitsResponseBody struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetCurrencyUnitsResponse"`

	GetCurrencyUnitsResult string `xml:"GetCurrencyUnitsResult,omitempty"`
}

type GetCurrentExchangeRatesRequestBody struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetCurrentExchangeRatesRequest"`
}

type GetCurrentExchangeRatesResponse struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetCurrentExchangeRatesResponse"`

	GetCurrentExchangeRatesResult []byte `xml:"GetCurrentExchangeRatesResult,omitempty"`
}

type GetDateIntervalRequestBody struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetDateIntervalRequest"`
}

type GetDateIntervalResponseBody struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetDateIntervalResponse"`

	GetDateIntervalResult string `xml:"GetDateIntervalResult,omitempty"`
}

type GetExchangeRatesRequestBody struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetExchangeRates"`

	StartDate     string `xml:"startDate,omitempty"`
	EndDate       string `xml:"endDate,omitempty"`
	CurrencyNames string `xml:"currencyNames,omitempty"`
}

type GetExchangeRatesResponseBody struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetExchangeRatesResponse"`

	GetExchangeRatesResult []byte `xml:"GetExchangeRatesResult,omitempty"`
}

type GetInfoRequestBody struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetInfoRequest"`
}

type GetInfoResponseBody struct {
	XMLName xml.Name `xml:"http://www.mnb.hu/webservices/ GetInfoResponse"`

	GetInfoResult string `xml:"GetInfoResult,omitempty"`
}

type MNBArfolyamServiceSoap struct {
	client *SOAPClient
}

func NewMNBArfolyamServiceSoap(url string, tls bool, auth *BasicAuth) *MNBArfolyamServiceSoap {
	if url == "" {
		url = ""
	}
	client := NewSOAPClient(url, tls, auth)

	return &MNBArfolyamServiceSoap{
		client: client,
	}
}

// Error can be either of the following types:
//
//   - StringFault

func (service *MNBArfolyamServiceSoap) GetCurrencies(request *GetCurrenciesRequestBody) (*MNBCurrencies, error) {
	response := new(GetCurrenciesResponse)
	err := service.client.Call("http://www.mnb.hu/webservices/MNBArfolyamServiceSoap/GetCurrencies", request, response)

	if err != nil {
		return nil, err
	}

	var luann MNBCurrencies
	xmlError := xml.Unmarshal(response.GetCurrenciesResult, &luann)
	if xmlError == nil {
		log.Debugf("unmarshall seems to be ok, some data as result")
		return &luann, nil
	}

	return nil, xmlError
}

// Error can be either of the following types:
//
//   - StringFault

func (service *MNBArfolyamServiceSoap) GetCurrencyUnits(request *GetCurrencyUnitsRequestBody) (*GetCurrencyUnitsResponseBody, error) {
	response := new(GetCurrencyUnitsResponseBody)
	err := service.client.Call("http://www.mnb.hu/webservices/MNBArfolyamServiceSoap/GetCurrencyUnits", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// Error can be either of the following types:
//
//   - StringFault

func (service *MNBArfolyamServiceSoap) GetCurrentExchangeRates(request *GetCurrentExchangeRatesResponse) (*MNBCurrentExchangeRates, error) {
	response := new(GetCurrentExchangeRatesResponse)
	err := service.client.Call("http://www.mnb.hu/webservices/MNBArfolyamServiceSoap/GetCurrentExchangeRates", request, response)
	if err != nil {
		return nil, err
	} else {
		if response != nil {
			// try to encode..
			var luann MNBCurrentExchangeRates
			xmlError := xml.Unmarshal(response.GetCurrentExchangeRatesResult, &luann)
			if xmlError != nil {
				return nil, xmlError
			} else {
				return &luann, nil
			}

		}
	}
	return nil, errors.New("no data returned")
}

// Error can be either of the following types:
//
//   - StringFault

func (service *MNBArfolyamServiceSoap) GetDateInterval(request *GetDateIntervalRequestBody) (*GetDateIntervalResponseBody, error) {
	response := new(GetDateIntervalResponseBody)
	err := service.client.Call("http://www.mnb.hu/webservices/MNBArfolyamServiceSoap/GetDateInterval", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// Error can be either of the following types:
//
//   - StringFault

func (service *MNBArfolyamServiceSoap) GetExchangeRates(request *GetExchangeRatesRequestBody) (*MNBExchangeRates, error) {
	response := new(GetExchangeRatesResponseBody)
	err := service.client.Call("http://www.mnb.hu/webservices/MNBArfolyamServiceSoap/GetExchangeRates", request, response)
	if err != nil {
		return nil, err
	}

	var luann MNBExchangeRates
	xmlError := xml.Unmarshal(response.GetExchangeRatesResult, &luann)
	if xmlError != nil {
		return nil, xmlError
	}

	return &luann, nil
}

// Error can be either of the following types:
//
//   - StringFault

func (service *MNBArfolyamServiceSoap) GetInfo(request *GetInfoRequestBody) (*GetInfoResponseBody, error) {
	response := new(GetInfoResponseBody)
	err := service.client.Call("http://www.mnb.hu/webservices/MNBArfolyamServiceSoap/GetInfo", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

var timeout = time.Duration(30 * time.Second)

func dialTimeout(network, addr string) (net.Conn, error) {
	return net.DialTimeout(network, addr, timeout)
}

type SOAPEnvelope struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`

	Body SOAPBody
}

type SOAPHeader struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Header"`

	Header interface{}
}

type SOAPBody struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`

	Fault   *SOAPFault  `xml:",omitempty"`
	Content interface{} `xml:",omitempty"`
}

type SOAPFault struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Fault"`

	Code   string `xml:"faultcode,omitempty"`
	String string `xml:"faultstring,omitempty"`
	Actor  string `xml:"faultactor,omitempty"`
	Detail string `xml:"detail,omitempty"`
}

type BasicAuth struct {
	Login    string
	Password string
}

type SOAPClient struct {
	url  string
	tls  bool
	auth *BasicAuth
}

func (b *SOAPBody) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	if b.Content == nil {
		return xml.UnmarshalError("Content must be a pointer to a struct")
	}

	var (
		token    xml.Token
		err      error
		consumed bool
	)

Loop:
	for {
		if token, err = d.Token(); err != nil {
			return err
		}

		if token == nil {
			break
		}

		switch se := token.(type) {
		case xml.StartElement:
			if consumed {
				return xml.UnmarshalError("Found multiple elements inside SOAP body; not wrapped-document/literal WS-I compliant")
			} else if se.Name.Space == "http://schemas.xmlsoap.org/soap/envelope/" && se.Name.Local == "Fault" {
				b.Fault = &SOAPFault{}
				b.Content = nil

				err = d.DecodeElement(b.Fault, &se)
				if err != nil {
					return err
				}

				consumed = true
			} else {
				if err = d.DecodeElement(b.Content, &se); err != nil {
					return err
				}

				consumed = true
			}
		case xml.EndElement:
			break Loop
		}
	}

	return nil
}

func (f *SOAPFault) Error() string {
	return f.String
}

func NewSOAPClient(url string, tls bool, auth *BasicAuth) *SOAPClient {
	return &SOAPClient{
		url:  url,
		tls:  tls,
		auth: auth,
	}
}

func (s *SOAPClient) Call(soapAction string, request, response interface{}) error {
	envelope := SOAPEnvelope{
	//Header:        SoapHeader{},
	}

	envelope.Body.Content = request
	buffer := new(bytes.Buffer)

	encoder := xml.NewEncoder(buffer)
	//encoder.Indent("  ", "    ")

	if err := encoder.Encode(envelope); err != nil {
		return err
	}

	if err := encoder.Flush(); err != nil {
		return err
	}

	log.Debug(buffer.String())

	req, err := http.NewRequest("POST", s.url, buffer)
	if err != nil {
		return err
	}
	if s.auth != nil {
		req.SetBasicAuth(s.auth.Login, s.auth.Password)
	}

	req.Header.Add("Content-Type", "text/xml; charset=\"utf-8\"")
	if soapAction != "" {
		req.Header.Add("SOAPAction", soapAction)
	}

	req.Header.Set("User-Agent", "gowsdl/0.1")
	req.Close = true

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: s.tls,
		},
		Dial: dialTimeout,
	}

	client := &http.Client{Transport: tr}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	rawbody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	if len(rawbody) == 0 {
		log.Warn("empty response")
		return nil
	}

	log.Debugf("raw-response: %s", string(rawbody))
	respEnvelope := new(SOAPEnvelope)
	respEnvelope.Body = SOAPBody{Content: response}
	err = xml.Unmarshal(rawbody, respEnvelope)
	if err != nil {
		return err
	}

	fault := respEnvelope.Body.Fault
	if fault != nil {
		return fault
	}

	return nil
}



// this function queries the MNB api for supported currencies
func GetCurrencyListing() ([]string, error) {
	log.Debug("no database in reach, fallback to mnb per default")
	sc := NewMNBArfolyamServiceSoap("http://www.mnb.hu/arfolyamok.asmx", false, nil)

	cresponse, err := sc.GetCurrencies(nil)

	if err != nil {
		return nil, err
	}
	if cresponse != nil {
		log.Debugf("currencies got from MNB, currency count: %d", len(cresponse.CurrencyList.Currency))
		return cresponse.CurrencyList.Currency, nil
	} else {
		return nil, errors.New("No data received from MNB call")
	}
}
